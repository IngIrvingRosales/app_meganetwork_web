<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<html lang="ES">
<head>
<title>MegaNetWork</title>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="../layout/styles/layout.css" rel="stylesheet"
	type="text/css" media="all">
</head>
<script type="text/javascript">
	function addCustomer() {
		// 	document.getElementById("logo").style.display ="none";
		document.forms.frmPrincipal.action = "Customer.jsp";
		document.forms.frmPrincipal.submit();
	}
	function showCustomer() {
		// 	document.getElementById("logo").style.display ="none";
		document.forms.frmPrincipal.action = "showCustomer.jsp";
		document.forms.frmPrincipal.submit();
	}
	function complaint() {
		// 	document.getElementById("logo").style.display ="none";
		document.forms.frmPrincipal.action = "Complaint.jsp";
		document.forms.frmPrincipal.submit();
	}
	function payments() {
		// 	document.getElementById("logo").style.display ="none";
		document.forms.frmPrincipal.action = "Payments.jsp";
		document.forms.frmPrincipal.submit();
	}
</script>
<body id="top">

	<div class="wrapper row1">
		<header id="header" class="hoc clear">
			<div id="logo" class="fl_left">
				<!-- ################################################################################################ -->
				<h1>
					<a href="index.html">MEGANETWORK</a>
				</h1>
				<!-- ################################################################################################ -->
			</div>
			<nav id="mainav" class="fl_right">
				<!-- ################################################################################################ -->
				<ul class="clear">
					<li class="active"><a href="index.jsp">Inicio </a></li>
					<li><a class="drop" href="#">Cliente </a>
						<ul>
							<li><a href="javascript:void(0);" target="framePrincipal" onclick="addCustomer();">Alta</a></li>
							<li><a href="javascript:void(0);" target="framePrincipal" onclick="showCustomer();">Consulta</a></li>
							<li><a href="javascript:void(0);" target="framePrincipal" onclick="complaint();">Quejas</a></li>
							<li><a href="javascript:void(0);" target="framePrincipal" onclick="payments();">Pagos</a></li>
						</ul>
					</li>
					<li><a class="drop" href="#">Reportes</a>
						<ul>
							<li><a href="#">Reporte Diario</a></li>
						</ul>
				</ul>
			</nav>
		</header>
	</div>
	<div id="principal">
		<form method="post" id="frmPrincipal" name="frmPrincipal" target="framePrincipal"></form>
		<iframe id="framePrincipal" style="border: none; margin: 0 auto;" src="" name="framePrincipal" height="100%" width="100%"> </iframe>
	</div>

	<!-- JAVASCRIPTS -->
	<script src="../layout/scripts/jquery.min.js"></script>
	<script src="../layout/scripts/jquery.backtotop.js"></script>
	<script src="../layout/scripts/jquery.mobilemenu.js"></script>
</body>
</html>