<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String name = request.getParameter("name") == null ? "" : (String) request.getParameter("name");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
div#inferior {
	margin: 0px;
	border: 0px;
	padding: 0px;
	left: 0px;
	top: 0px;
	overflow: auto;
	position: absolute;
	vertical-align: middle;
}

div#interno {
	margin: 0px;
	border: 0px;
	padding: 0px;
	z-index: 1;
	position: absolute;
	overflow: hidden;
	_position: absolute;
	top: 0px;
	white-space: nowrap;
}

html body, form {
	margin: 0px;
	padding: 0px;
	border: 0px;
	overflow: hidden;
}

th {
	background: url(../layout/styles/images/patron.png) repeat-x 5px;
	padding: 0px;
	text-align: left;
	border-left: 1px solid #CCC;
	border-right: none;
	font-size: 10px;
	font-weight: bold;
	height: 35px;
	color: white;
}

tr.normalRow td {
	background: #FFF;
}

tr.alternateRow td {
	background: #EEE;
}

tr.tomadoRow td {
	background-color: yellow;
	color: #FF0000;
}

td {
	font-size: 10px;
	padding: 1px 2px 2px 3px;
	border-left: 1px solid #CCC;
	border-right: none;
	border-bottom: 1px solid #DDD;
}

table {
	border-right: 1px solid #CCC;
	font-family: Verdana, Arial, Helvetica, sans-serif;
}

a, a:link, a:visited {
	color: #4166cc;
	text-decoration: none;
	_width: 100%;
}

a:hover {
	text-decoration: underline;
}

.boton {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12pts;
	color: #023f9b;
	cursor: hand;
}
</style>
<link href="../layout/styles/formstyle.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" href="../layout/styles/esperaModal.css">
<script type="text/javascript" src="../layout/scripts/esperaModal.js"></script>
<script type="text/javascript"
	src="../layout/scripts/jquery-1.2.6.min.js"></script>

<script language="javascript" type="text/javascript">

var numPagina=0;
var numRegistros=0;
var xmlHttp, xml;// global instance of XMLHttpRequest
var ajax, jx;// global instance of XMLHttpRequest


 		$(document).ready(function(){ 
 			$(window)
 				.load(function(){
 					dimencionaWin();
 					scrollInfe();
 					dimencionaInfer();
 				})
 				.resize(function(){
 					dimencionaWin();
 					scrollInfe();
 					dimencionaInfer();
 				}); 
 			$("div#inferior").scroll(function () {scrollInfe();}); 
 			$("div#inferior").resize(function(){dimencionaInfer();}); 
 		});
 		
	function dimencionaWin(){
		var hWindow = $(window).height();                                                    
		var wWindow = $(window).width();                                                    
		$("div#inferior").css({height:hWindow,width:wWindow,overflow:"auto"});	
  	}                                                                                    
  	                                                                                     
  	function scrollInfe(){
		var sInf = $("div#inferior");                                                        
		$("div#interno").css({left:"-"+sInf.scrollLeft()+"px",display:"inline"});            
		var wInterno = $(window).width()-16;                                                 
		$("div#interno").css({width:wInterno+sInf.scrollLeft()+"px"});				  	           
  	}                                                                                  
  	                                                                                     
  	function dimencionaInfer(){                                                          
        var wTable = $("table#tablaDoc").width();  
        var wTableEncabeza = $("table#tablaDocEncabeza").width();                       
                                                                                        
      	if(wTable>wTableEncabeza){                                                 
      		$("table#tablaDocEncabeza").css("width",wTable);                               
      	}else{                                                                           
      		$("table#tablaDoc").css("width",wTableEncabeza);                               
      	}                                                                                
      	                                                                          
      	var a = [0,1,2,3,4,5,6,7,8,9,10,11];                                                     
		jQuery.each(a, function() {                                                          
			try{                                                                               
				var leftTit = $("th#tit"+this).get(0).offsetLeft;                                
				var widthTit = $("th#tit"+this).get(0).offsetWidth;                              
				var heightTit = $("th#tit"+this).get(0).offsetHeight;                            
			}catch(e){}	                                                                       
			if (leftTit!=undefined&&widthTit!=undefined&&heightTit!=undefined){                
				$("th#titVista"+this).css({left:leftTit, width:widthTit, height:heightTit});     
			}                                                                                  
		});                                                                           
	}
		                                                                             
// 	function blockButtonBuscar(bloquea){
// 		if(bloquea){
// 			parent.framePrincipal.document.getElementById('BuscarButton').disabled = true;
// 			parent.framePrincipal.document.getElementById('BuscarButton').className = "boton";
// 		}else{
// 			parent.framePrincipal.document.getElementById('BuscarButton').disabled = false;
// 			parent.framePrincipal.document.getElementById('BuscarButton').className = "enabled";
// 		}

// 	}

	function inicializa(){
// 		blockButtonBuscar(true);
		soloesperarGeneral();
		numPagina++;
		createXmlHttpRequest();
		var uu="../SearchCustomer?name=<%=name%>&numPage="+numPagina+"&rand="+Math.random();
		ajax.open("GET",uu,true);
		ajax.onreadystatechange=consulta;
		ajax.send(null);
	}

	function PaginaAnterior( ){
// 		blockButtonBuscar(true);
		soloesperarGeneral();
		if(numPagina==1){
			alert("No hay datos previos");
			remove_soloesperarGeneral();
// 			blockButtonBuscar(false);
		}else{
			numPagina-=1;
			createXmlHttpRequest();
			var uu="../SearchCustome?name=<%=name%>&numPage="+numPagina+"&rand="+Math.random();
			ajax.open("GET",uu,true);
			ajax.onreadystatechange=consulta;
			ajax.send(null);
		}
	}

	function  PaginaSiguiente(){
// 		blockButtonBuscar(true);
		soloesperarGeneral();
		if(numRegistros<500){
			alert("No hay mas registros");
			remove_soloesperarGeneral();
// 			blockButtonBuscar(false);
		}else{
			numRegistros=0;
			numPagina+=1;
			createXmlHttpRequest();
			var uu="../SearchCustomer?name=<%=name%>&numPage="+numPagina+"&rand="+Math.random();
			ajax.open("GET", uu, true);
			ajax.onreadystatechange = consulta;
			ajax.send(null);
		}
	}

	function createXmlHttpRequest() {
		var bActiveX = false;
		try { // Firefox, Opera 8.0+, Safari
			xmlHttp = new XMLHttpRequest();
			ajax = new XMLHttpRequest();
		} catch (e) {
			try { // Internet Explorer
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
				ajax = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try {
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
					ajax = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {
					alert("Su browser no suporta AJAX!");
					return false;
				}
			}
		}
	}

	function consulta() {
		if (ajax.readyState == 4) {
			if (ajax.status == 200) {
				var salida = ajax.responseText;
				var inicio = 8; // la respuesta inicia con <codigo> = 8
				var fin = salida.lastIndexOf("</codigo>");
				var aux = salida.substring(inicio, fin);
				if (aux == -1) { //Session no valida
					alert("Sesion no valida");
					top.location.href = "../Salir.jsp";
				} else {
					if (aux == -2) {
						alert("Usuario no valido");
						top.location.href = "../Salir.jsp";
					} else {
						if (aux == -3) {
							alert("Error de comunicacion");
							remove_soloesperarGeneral();
// 							blockButtonBuscar(false);
						} else {
							numRegistros = aux
							if (aux == -4) {
								alert("La busqueda es muy grande favor de delimitarla");
								remove_soloesperarGeneral();
// 								blockButtonBuscar(false);
							} else {
								aux = salida.substring(fin + 10);
								document.getElementById("nn").innerHTML = aux;
								stripedTable();
								dimencionaWin();
								scrollInfe();
								dimencionaInfer();
								remove_soloesperarGeneral();
// 								blockButtonBuscar(false);
							}
						}
					}
				}
			}
		}
	}

	function removeClassName(elem, className) {
		elem.className = elem.className.replace(className, "").trim();
	}

	function addCSSClass(elem, className) {
		removeClassName(elem, className);
		elem.className = (elem.className + " " + className).trim();
	}

	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/, "");
	}

	function stripedTable() {
		if (document.getElementById && document.getElementsByTagName) {
			var allTables = document.getElementsByTagName('table');
			if (!allTables) {
				return;
			}

			for (var i = 0; i < allTables.length; i++) {
				if (allTables[i].className.match(/[\w\s ]*tablaDatos[\w\s ]*/)) {
					var trs = allTables[i].getElementsByTagName("tr");
					for (var j = 0; j < trs.length; j++) {
						removeClassName(trs[j], 'alternateRow');
						addCSSClass(trs[j], 'normalRow');
					}
					for (var k = 0; k < trs.length; k += 2) {
						removeClassName(trs[k], 'normalRow');
						addCSSClass(trs[k], 'alternateRow');
					}
				}
			}
		}
	}
</script>
</head>
<body onload="javascript:inicializa();">
	<div id="interno">
		<table id="tablaDocEncabeza" cellpadding="0" cellspacing="0"
			border="0">
			<thead>
				<tr height="35px">
					<th id="titVista0">&nbsp;Registro</th>
					<th id="titVista1">&nbsp;Nombre&nbsp;</th>
					<th id="titVista2">&nbsp;Dirección&nbsp;</th>
					<th id="titVista3">&nbsp;Localidad&nbsp;</th>
					<th id="titVista4">&nbsp;Telefono&nbsp;</th>
					<th id="titVista5">&nbsp;Referencia&nbsp;</th>
					<th id="titVista6">&nbsp;Altitud&nbsp;</th>
					<th id="titVista7">&nbsp;Longitud&nbsp;</th>
					<th id="titVista8">&nbsp;Tipo de Servicio&nbsp;</th>
					<th id="titVista9">&nbsp;MAC&nbsp;</th>
					<th id="titVista10">&nbsp;Equipo&nbsp;</th>
					<th id="titVista11">&nbsp;Paquete&nbsp;</th>
				</tr>
			</thead>
		</table>
	</div>
	<div id="inferior">
		<div id="nn"></div>
		<div class="row" align="center">
			<br>
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td align="right">
						<input type="button" onclick="javascript:PaginaAnterior();" class="boton" value="Anterior"> 
						<input type="button" onclick="javascript:PaginaSiguiente();" class="boton" value="Siguiente">
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>