<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="../layout/styles/formstyle.css" rel="stylesheet" type="text/css" media="all">
</head>
<body>


<div class="container">
  <form method="post" action="../AddCustomer">
<!--   <h1 align="center">ALTA DE CLIENTE</h1> -->
  <div class="row">
    <div class="col-25">
      <label for="fname"><b>Nombre:</b></label>
    </div>
    <div class="col-75">
      <input type="text" id="name" name="name" placeholder="Ingresa el nombre">
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="lname"><b>Direcci�n:</b></label>
    </div>
    <div class="col-75">
      <input type="text" id="direction" name="direction" placeholder="Ingresa la direcci�n">
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="country"><b>Localidad:</b></label>
    </div>
    <div class="col-75">
      <input type="text" id="location" name="location" placeholder="Ingresa la localidad">
    </div>
  </div>
  <div class="row">
   	<div class="col-25">
      <label for="lname"><b>Telefono:</b></label>
    </div>
    <div class="col-75">
      <input type="text" id="phone" name="phone" placeholder="Ingresa el telefono">
    </div>
  </div>
  <div class="row">
  <div class="col-25">
      <label for="lname"><b>Referencia:</b></label>
    </div>
    <div class="col-75">
      <input type="text" id="reference" name="reference" placeholder="Ingresa la referencia">
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="lname"><b>Altitud:</b></label>
    </div>
    <div class="col-75">
      <input type="text" id="altitude" name="altitude" placeholder="Ingresa la altitud">
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="lname"><b>Longitud:</b></label>
    </div>
    <div class="col-75">
      <input type="text" id="longitude" name="longitude" placeholder="Ingresa la longitud">
  	</div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="lname"><b>Tipo de Servicio:</b></label>
    </div>
    <div class="col-75">
      <input type="text" id="type_service" name="type_service" placeholder="Ingresa el tipo de servicio">
    </div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="lname"><b>MAC:</b></label>
    </div>
    <div class="col-75">
      <input type="text" id="mac_device" name="mac_device" placeholder="Ingresa el MAC Address">
  	</div>
  </div>
  <div class="row">
  	<div class="col-25">
      <label for="lname"><b>Equipo:</b></label>
    </div>
    <div class="col-75">
      <input type="text" id="equipment" name="equipment" placeholder="Ingresa el equipo">
  	</div>
  </div>
  <div class="row">
    <div class="col-25">
      <label for="country"><b>Paquete:</b></label>
    </div>
    <div class="col-75">
	   	<select id="pack" name="pack">
	     	<option value="paquete300">300</option>
	     	<option value="paquete400">400</option>
	     	<option value="paquete500">500</option>
	     	<option value="paquete600">600</option>
	     	<option value="otro">OTRO</option>
	   	</select>
	 </div>	
   </div>
  <br>
  <div class="row">
    <input type="submit" value="Guardar">
  </div>
  </form>
</div>




<!-- 	<h1 align="center">ALTA DE CLIENTE</h1> -->
<!-- 	<div> -->

<!-- 		<form action="./pages/MainPage.jsp"> -->
<!-- 			<table align="center"> -->
<!-- 			<tr> -->
<!-- 				<td align="right" style="color: #184d97; font-family:Tahoma, sans-serif;">Nombre:</td> -->
<!-- 				<td><input type="text" value="" id="name" size="50" placeholder="Ingresa el nombre"/> </td> -->
<!-- 			</tr> -->
<!-- 			<tr> -->
<!-- 				<td align="right" style="color: #184d97; font-family:Tahoma, sans-serif;">Direccion:</td> -->
<!-- 				<td><input type="text" value="" id="direction" size="50" placeholder="Ingresa la direcci�n"/></td> -->
<!-- 			</tr> -->
<!-- 			<tr> -->
<!-- 				<td align="right" style="color: #184d97; font-family:Tahoma, sans-serif;">Localidad:</td> -->
<!-- 				<td><input type="text" value="" id="location" placeholder="Ingresa la localidad"/></td> -->
<!-- 			</tr> -->
<!-- 			<tr> -->
<!-- 				<td align="right" style="color: #184d97; font-family:Tahoma, sans-serif;">Telefono:</td> -->
<!-- 				<td><input type="text" value="" id="phone" placeholder="Ingresa el telefono"/></td> -->
<!-- 			</tr> -->
<!-- 			<tr> -->
<!-- 				<td align="right" style="color: #184d97; font-family:Tahoma, sans-serif;">Referencia:</td> -->
<!-- 				<td><input type="text" value="" id="reference" placeholder="Ingresa la referencia"/></td> -->
<!-- 			</tr> -->
<!-- 			<tr> -->
<!-- 				<td align="right" style="color: #184d97; font-family:Tahoma, sans-serif;">Altitud:</td> -->
<!-- 				<td><input type="text" value="" id="altitude" placeholder="Ingresa la altitud"/></td> -->
<!-- 			</tr> -->
<!-- 			<tr> -->
<!-- 				<td align="right" style="color: #184d97; font-family:Tahoma, sans-serif;">Longitud:</td> -->
<!-- 				<td><input type="text" value="" id="longitude" placeholder="Ingresa la longitud"/></td> -->
<!-- 			</tr> -->
<!-- 			<tr> -->
<!-- 				<td align="right" style="color: #184d97; font-family:Tahoma, sans-serif;">Tipo de Servicio:</td> -->
<!-- 				<td><input type="text" value="" id="type_service" placeholder="Ingresa el tipo de servicio"/></td> -->
<!-- 			</tr> -->
<!-- 			<tr> -->
<!-- 				<td align="right" style="color: #184d97; font-family:Tahoma, sans-serif;">MAC Address:</td> -->
<!-- 				<td><input type="text" value="" id="mac_device" placeholder="Ingresa el MAC Address"/></td> -->
<!-- 			</tr> -->
<!-- 			<tr> -->
<!-- 				<td align="right" style="color: #184d97; font-family:Tahoma, sans-serif;">Equipo:</td> -->
<!-- 				<td><input type="text" value="" id="equipment" placeholder="Ingresa el equipo"/></td> -->
<!-- 			</tr>	  -->
<!-- 			<tr> -->
<!-- 				<td align="right" style="color: #184d97; font-family:Tahoma, sans-serif;">Paquete Contratado:</td> -->
<!-- 				<td><input type="text" value="" id="package" placeholder="Ingresa el paquete"/></td> -->
<!-- 			</tr> -->
<!-- 			<tr> -->
<!-- 				<td></td> -->
<!-- 				<td><button>Guardar</button></td> -->
<!-- 			</tr>			 -->
<!-- 			</table> -->
			
<!-- 		</form> -->
<!-- 	</div> -->

</body>
</html>