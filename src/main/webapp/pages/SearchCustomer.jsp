<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="../layout/styles/formstyle.css" rel="stylesheet" type="text/css" media="all">

<style type="text/css">
.Estilo1 {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: bold;
	color: #316BA2;
}

span.Estilo2 {
	font: bold 14px Verdana, Geneva, Arial, Helvetica, sans-serif;
	font-size: 11pt;
	color: #316BA2;
}

.boton {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11pt;
	color: #023f9b;
	/*cursor: hand;*/
}

input.enabled {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11pt;
	color: #023f9b;
	/*cursor: hand;*/
}

input.disabled {
	background: #CCCCCC;
	border: 1px solid #999999;
}

.panel-heading {
	padding: 5px 15px;
	border-top-left-radius: 3px;
	border-top-right-radius: 3px;
	background-color: rgb(8, 97, 176);
	position: fixed;
	width: 100%;
	z-index: 10005;
	left: 0px;
	top: 0;
}

.panel-title {
	margin-top: 0;
	margin-bottom: 0;
	font-size: 14px;
	color: white;
	text-align: left;
}

.panel-title>a {
	color: inherit;
}

h4 {
	font-family: inherit;
	font-weight: 500;
	line-height: 1.1;
}

a {
	color: #337ab7;
	text-decoration: none;
}
</style>

<script type="text/javascript">
	function limpia() {
		document.getElementById("name").value = "";
// 		document.getElementById('BuscarButton').disabled = false;
// 		document.getElementById('BuscarButton').className = "enabled";
	}
	
	
	
</script>
</head>
<body>
	<div class="container">
		<form action="ResultCustomer.jsp" method="post" id="buscar" target="frameResultados">
			<div class="row">
				<div class="col-25">
					<label for="lname"><b>Nombre Cliente:</b></label>
				</div>
				<div class="col-75">
					<input type="text" id="name" name="name"
						placeholder="Ingresa el nombre del cliente">
				</div>
			</div>

			<div class="row">
				<input type="submit" value="Buscar"> 
				<input name="Limpiar" type="button" value="Limpiar" onclick="limpia();" />
			</div>
		</form>
	</div>
</body>
</html>