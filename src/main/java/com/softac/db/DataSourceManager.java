package com.softac.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class DataSourceManager extends Thread {
	
	private static DataSourceManager dsm = null;
	private static String password_db = "12345678";
	private static String user_db = "root";
	
	
	public Connection getConnection() throws SQLException {

		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/meganetwork?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
					user_db, password_db);
			System.out.println("Connected");
		} catch (Exception e) {
			System.out.println(e);
		}

		conn.setAutoCommit(false);
		return conn;
	}

	public static Connection getConnectionStatic() throws SQLException {
		if (dsm == null)
			dsm = new DataSourceManager() {};
			
		return dsm.getConnection();
	}
}