package com.softac.dao;

import static java.lang.String.format;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.softac.bean.CustomerBean;
import com.softac.db.DataSourceManager;
import com.softac.manager.SequenceManager;

public class CustomerDao extends DataSourceManager{

	public void addCustomer(String name, String direction, String location, String phone, String reference, String altitude, 
			String longitude, String type_service, String mac_device, String equipement, String pack) {
		Connection conn = null;
		PreparedStatement pstmnt = null;
		try {
			conn = getConnection();
			String queryInsert = "INSERT INTO CUSTOMER VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
			pstmnt = conn.prepareStatement(queryInsert);
			pstmnt.setInt(1, getNewId(conn, "customer", "id_customer"));
			pstmnt.setString(2, name);
			pstmnt.setString(3, direction);
			pstmnt.setString(4, location);
			pstmnt.setString(5, phone);
			pstmnt.setString(6, reference);
			pstmnt.setString(7, altitude);
			pstmnt.setString(8, longitude);
			pstmnt.setString(9, type_service);
			pstmnt.setString(10, mac_device);
			pstmnt.setString(11, equipement);
			pstmnt.setString(12, pack);
			pstmnt.execute();
			conn.commit();
		} catch (SQLException e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
			try {
				if (pstmnt != null)
					pstmnt.close();
				
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public List<CustomerBean> findCustomer(String name) {
		Connection conn = null;
		PreparedStatement pstmnt = null;
		ResultSet rs = null;
		List<CustomerBean> listcCustomer = new ArrayList<CustomerBean>();
		
		String querySelect = null;
		try {
			conn = getConnection();
			
			if(name.isEmpty()){
				querySelect = "SELECT ID_CUSTOMER, NAME, DIRECTION, LOCATION, PHONE, REFERENCE, ALTITUDE, LONGITUDE, TYPE_SERVICE, MAC_DEVICE, EQUIPMENT, PACKAGE "
						+ " FROM CUSTOMER ";
			}else {
				querySelect = "SELECT ID_CUSTOMER, NAME, DIRECTION, LOCATION, PHONE, REFERENCE, ALTITUDE, LONGITUDE, TYPE_SERVICE, MAC_DEVICE, EQUIPMENT, PACKAGE "
						+ " FROM CUSTOMER WHERE NAME LIKE ?";
			}
			pstmnt = conn.prepareStatement(querySelect);
			if(!name.isEmpty()){
				pstmnt.setString(1, "%" +name+ "%");
			}
			pstmnt.execute();
			rs = pstmnt.getResultSet();
			while (rs.next()) {
				CustomerBean cbean = new CustomerBean();
				cbean.setName(rs.getString("name"));
				cbean.setDirection(rs.getString("direction"));
				cbean.setLocation(rs.getString("location"));
				cbean.setPhone(rs.getString("phone"));
				cbean.setReference(rs.getString("reference"));
				cbean.setAltitude(rs.getString("altitude"));
				cbean.setLongitude(rs.getString("longitude"));
				cbean.setType_service(rs.getString("type_service"));
				cbean.setMac_device(rs.getString("mac_device"));
				cbean.setEquipment(rs.getString("equipment"));
				cbean.setPack(rs.getString("package"));
//				cbean.setPago(calculateStatusPay(rs.getInt("id_customer")));
				listcCustomer.add(cbean);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if (pstmnt != null)
					pstmnt.close();
				
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return listcCustomer;
	}
	
	public String calculateStatusPay(int id_customer) {
		Connection conn = null;
		PreparedStatement pstmnt = null;
		ResultSet rs = null;
		String msgReturn = null;;
		
		String querySelect = null;  
		try {
			conn = getConnection();
			
	
			querySelect = "SELECT ID_PAYMENT, ID_CUSTOMER, DESCRIPTION, COST, DATE_PAYMENT, ID_TICKET, MONTH_PAYMENT "
						+ " FROM PAYMENT WHERE ID_CUSTOMER = ?";
			
			pstmnt = conn.prepareStatement(querySelect);
			if(id_customer != -1){
				pstmnt.setInt(1, id_customer);
			}
			pstmnt.execute();
			rs = pstmnt.getResultSet();
//			if (rs.next()) {
//				existe = true;
//			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if (pstmnt != null)
					pstmnt.close();
				
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return msgReturn;
	}
	
	public void deleteCustomer() {
		System.out.println("Falta implementar");
	}
	
	public void updateCustomer() {
		System.out.println("Falta implementar");
	}
	
	public void selarchCustomer() {
		System.out.println("Falta implementar");
	}
	
	private static synchronized int getNewId(Connection conn, String tabla, String campo) throws SQLException {

		if (tabla == null)
			throw new IllegalArgumentException("tabla_aplicacion null");

		if (tabla.trim().length() == 0)
			throw new IllegalArgumentException("tabla_aplicacion vacio");

		return SequenceManager.getInstance().nextValue(conn, format(tabla), campo, format("seq_%s", tabla).toUpperCase());
	}
}
