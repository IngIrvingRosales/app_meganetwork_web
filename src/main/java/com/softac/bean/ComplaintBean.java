package com.softac.bean;

public class ComplaintBean {

	private int id_complaint;
	private int id_customer;
	private int description;
	public int getId_complaint() {
		return id_complaint;
	}
	public void setId_complaint(int id_complaint) {
		this.id_complaint = id_complaint;
	}
	public int getId_customer() {
		return id_customer;
	}
	public void setId_customer(int id_customer) {
		this.id_customer = id_customer;
	}
	public int getDescription() {
		return description;
	}
	public void setDescription(int description) {
		this.description = description;
	}
	
	
}
