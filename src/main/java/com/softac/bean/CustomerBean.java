package com.softac.bean;

public class CustomerBean {

	private String name;
	private String direction;
	private String location; 
	private String phone;
	private String reference; 
	private String altitude;
	private String longitude; 
	private String type_service;
	private String mac_device;
	private String equipment;
	private String pack;
	
	private String pago;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		if(name == null)
			name = "";
		this.name = name;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		if(direction == null)
			direction = "";
		this.direction = direction;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		if(location == null)
			location = "";
		this.location = location;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		if(phone == null)
			phone = "";
		this.phone = phone;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		if(reference == null)
			reference = "";
		this.reference = reference;
	}
	public String getAltitude() {
		return altitude;
	}
	public void setAltitude(String altitude) {
		if(altitude == null)
			altitude = "";
		this.altitude = altitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		if(longitude == null)
			longitude = "";
		this.longitude = longitude;
	}
	public String getType_service() {
		return type_service;
	}
	public void setType_service(String type_service) {
		if(type_service == null)
			type_service = "";
		this.type_service = type_service;
	}
	public String getMac_device() {
		return mac_device;
	}
	public void setMac_device(String mac_device) {
		if(mac_device == null)
			mac_device = "";
		this.mac_device = mac_device;
	}
	public String getEquipment() {
		return equipment;
	}
	public void setEquipment(String equipment) {
		if(equipment == null)
			equipment = "";
		this.equipment = equipment;
	}
	public String getPack() {
		return pack;
	}
	public void setPack(String pack) {
		if(pack == null)
			pack = "";
		this.pack = pack;
	}
	public String getPago() {
		return pago;
	}
	public void setPago(String pago) {
		this.pago = pago;
	}
	
	
}
