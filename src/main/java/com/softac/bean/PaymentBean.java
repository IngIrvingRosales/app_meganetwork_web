package com.softac.bean;

import java.sql.Date;

public class PaymentBean {

	private int id_payment;
	private int id_customer;
	private String description;
	private String cost;
	private Date date_payment;
	private int id_ticket;
	private String month_payment;
	public int getId_payment() {
		return id_payment;
	}
	public void setId_payment(int id_payment) {
		this.id_payment = id_payment;
	}
	public int getId_customer() {
		return id_customer;
	}
	public void setId_customer(int id_customer) {
		this.id_customer = id_customer;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public Date getDate_payment() {
		return date_payment;
	}
	public void setDate_payment(Date date_payment) {
		this.date_payment = date_payment;
	}
	public int getId_ticket() {
		return id_ticket;
	}
	public void setId_ticket(int id_ticket) {
		this.id_ticket = id_ticket;
	}
	public String getMonth_payment() {
		return month_payment;
	}
	public void setMonth_payment(String month_payment) {
		this.month_payment = month_payment;
	}
	
	
}
