package com.softac.bean;

public class ActionTypeBean {

	private int id_action_type;
	private String type_description;
	public int getId_action_type() {
		return id_action_type;
	}
	public void setId_action_type(int id_action_type) {
		this.id_action_type = id_action_type;
	}
	public String getType_description() {
		return type_description;
	}
	public void setType_description(String type_description) {
		this.type_description = type_description;
	}
	
	
}
