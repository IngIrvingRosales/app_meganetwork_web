package com.softac.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softac.manager.CustomerManager;

/**
 * Servlet implementation class AddCustomer
 */
@WebServlet(
		urlPatterns = { "/AddCustomer" }, 
		initParams = { 
				@WebInitParam(name = "AddCustomer", value = "", description = "Agregar clientes")
		})
public class AddCustomer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCustomer() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String direction = request.getParameter("direction");
		String location = request.getParameter("location");
		String phone = request.getParameter("phone");
		String reference = request.getParameter("reference");
		String altitude = request.getParameter("altitude");
		String longitude = request.getParameter("longitude");
		String type_service = request.getParameter("type_service");
		String mac_device = request.getParameter("mac_device");
		String equipment = request.getParameter("equipment");
		String pack = request.getParameter("pack");
		
		CustomerManager cm = new CustomerManager();
		cm.addCustomer(name, direction, location, phone, reference, altitude, longitude, type_service, mac_device, equipment, pack);
		
		response.getWriter().append("Se inserto correctamente el cliente: ")
		.append(name)
		.append(direction)
		.append(location)
		.append(phone)
		.append(reference)
		.append(altitude)
		.append(longitude)
		.append(type_service)
		.append(mac_device)
		.append(equipment)
		.append(pack);	
		
		
	}

}
