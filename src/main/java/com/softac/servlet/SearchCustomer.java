package com.softac.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softac.bean.CustomerBean;
import com.softac.manager.CustomerManager;

/**
 * Servlet implementation class SearchCustomer
 */
@WebServlet(
		urlPatterns = { "/SearchCustomer" }, 
		initParams = { 
				@WebInitParam(name = "SearchCustomer", value = "", description = "Busqueda de cliente")
		})
public class SearchCustomer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public static final int LIMIT_RESULT_CUSTOMER = 50;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchCustomer() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		//String numPage = request.getParameter("numPage");
		int numPage = Integer.parseInt((String) request.getParameter("numPage"));
		
		PrintWriter out = null;
		try {
			out = response.getWriter();
			response.setContentType("text/html; charset=ISO-8859-1");

//			HttpSession session = request.getSession(false);
//			if (session == null) {
//				out.println("<codigo>-1</codigo>");
//				out.flush();
//				return;
//			}
//
//			Usuario u = (Usuario) session.getAttribute(constSesion.ATT_USER);
//			if (u == null) {
//				out.println("<codigo>-2</codigo>");
//				out.flush();
//				return;
//			}
			
			List<CustomerBean> listCustomer = null;
			CustomerManager cm = new CustomerManager();
			listCustomer = cm.findCustomer(name);

			int tamanio = listCustomer.size();
			out.println("<codigo>" + tamanio + "</codigo>)");

			out.println("<table id=\"tablaDoc\"  width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" class=\"tablaDatos\"");
			out.println("<thead");
			
			out.println("	<tr id=\"encabezado\" height=\"35px\">");
			out.println("		<th id=\"tit0\" width=\"1%\">");
			out.println("			Registro");
			out.println("		</th>");
			out.println("		<th id=\"tit1\" width=\"10%\">");
			out.println("			Nombre");
			out.println("		<th id=\"tit2\" width=\"10%\">");
			out.println("			Dirección");
			out.println("		</th>");
			out.println("		<th id=\"tit3\" width=\"5%\">");
			out.println("			Localidad");
			out.println("		</th>");
			out.println("		<th id=\"tit4\" width=\"5%\">");
			out.println("			Telefono");
			out.println("		</th>");
			out.println("		<th id=\"tit5\" width=\"5%\">");
			out.println("			Referencia");
			out.println("		</th>");
			out.println("		<th id=\"tit6\" width=\"5%\">");
			out.println("			Altitud");
			out.println("		</th>");
			out.println("		<th id=\"tit7\" width=\"5%\">");
			out.println("			Latitud");
			out.println("		</th>");
			out.println("		<th id=\"tit8\" width=\"5%\">");
			out.println("			Tipo de Servicio");
			out.println("		</th>");
			out.println("		<th id=\"tit9\" width=\"5%\">");
			out.println("			MAC");
			out.println("		</th>");
			out.println("		<th id=\"tit10\" width=\"5%\">");
			out.println("			Equipo");
			out.println("		</th>");
			out.println("		<th id=\"tit11\" width=\"5%\">");
			out.println("			Paquete");
			out.println("		</th>");
			out.println("	</tr>");
			
			out.println("</thead>");
			out.println("<tbody>");

			long contador = ((numPage - 1) * LIMIT_RESULT_CUSTOMER) + 1;
			for (Iterator<CustomerBean> i = listCustomer.iterator(); i.hasNext(); contador++) {
				CustomerBean cb = (CustomerBean) i.next();
				out.println("<tr>");
				out.println("<td align=\"center\" style=\"font-weight:bold;\" width=\"1%\">");
				out.println("<a href=\"../HistoricoTree?Gabeton=" + contador + "\" target=\"_top\">" + contador + "</a>");
//				out.println("<a href=\"../HistoricoTree?Gabeton=" + hb.getIdGabinete() + "&Aplicacion=" + hb.getIdGaveta() + "&numcliente=" + hb.getNumCliente() + "&nomcliente=" + hb.getNomCliente() + "\" target=\"_top\">" + contador + "</a>");
				out.println("</td>");
				out.println("<td align=\"left\" width=\"10%\">");
				out.println("<a href=\"../HistoricoTree?Gabeton=" + cb.getName() + "\" target=\"_top\">" + (cb.getName().isEmpty() ? "" : cb.getName()) + "</a>");
//				out.println("<a href=\"../HistoricoTree?Gabeton=" + hb.getIdGabinete() + "&Aplicacion=" + hb.getIdGaveta() + "&numcliente=" + hb.getNumCliente() + "&nomcliente=" + hb.getNomCliente() + "\" target=\"_top\">" + hb.getNumCliente() + "</a>");
				out.println("</td>");
				out.println("<td align=\"left\" width=\"10%\">");
				out.println("<a href=\"../HistoricoTree?Gabeton=" + cb.getDirection() + "\" target=\"_top\">" + (cb.getDirection().isEmpty() ? "" : cb.getDirection()) + "</a>");
//				out.println("<a href=\"../HistoricoTree?Gabeton=" + hb.getIdGabinete() + "&Aplicacion=" + hb.getIdGaveta() + "&numcliente=" + hb.getNumCliente() + "&nomcliente=" + hb.getNomCliente() + "\" target=\"_top\">" + hb.getNomCliente() + "</a>");
				out.println("</td>");
				out.println("<td align=\"left\" width=\"5%\">");
				out.println("<a href=\"../HistoricoTree?Gabeton=" + cb.getLocation() + "\" target=\"_top\">" + (cb.getLocation().isEmpty() ? "" : cb.getLocation()) + "</a>");
//				out.println("<a href=\"../HistoricoTree?Gabeton=" + hb.getIdGabinete() + "&Aplicacion=" + hb.getIdGaveta() + "&numcliente=" + hb.getNumCliente() + "&nomcliente=" + hb.getNomCliente() + "\" target=\"_top\">" + hb.getNomCliente() + "</a>");
				out.println("</td>");
				out.println("<td align=\"left\" width=\"5%\">");
				out.println("<a href=\"../HistoricoTree?Gabeton=" + cb.getPhone() + "\" target=\"_top\">" + (cb.getPhone().isEmpty() ? "" : cb.getPhone()) + "</a>");
//				out.println("<a href=\"../HistoricoTree?Gabeton=" + hb.getIdGabinete() + "&Aplicacion=" + hb.getIdGaveta() + "&numcliente=" + hb.getNumCliente() + "&nomcliente=" + hb.getNomCliente() + "\" target=\"_top\">" + hb.getNomCliente() + "</a>");
				out.println("</td>");
				out.println("<td align=\"left\" width=\"5%\">");
				out.println("<a href=\"../HistoricoTree?Gabeton=" + cb.getReference() + "\" target=\"_top\">" + (cb.getReference().isEmpty() ? "" : cb.getReference()) + "</a>");
//				out.println("<a href=\"../HistoricoTree?Gabeton=" + hb.getIdGabinete() + "&Aplicacion=" + hb.getIdGaveta() + "&numcliente=" + hb.getNumCliente() + "&nomcliente=" + hb.getNomCliente() + "\" target=\"_top\">" + hb.getNomCliente() + "</a>");
				out.println("</td>");
				out.println("<td align=\"left\" width=\5%\">");
				out.println("<a href=\"../HistoricoTree?Gabeton=" + cb.getAltitude() + "\" target=\"_top\">" + (cb.getAltitude().isEmpty() ? "" : cb.getAltitude()) + "</a>");
//				out.println("<a href=\"../HistoricoTree?Gabeton=" + hb.getIdGabinete() + "&Aplicacion=" + hb.getIdGaveta() + "&numcliente=" + hb.getNumCliente() + "&nomcliente=" + hb.getNomCliente() + "\" target=\"_top\">" + hb.getNomCliente() + "</a>");
				out.println("</td>");
				out.println("<td align=\"left\" width=\"5%\">");
				out.println("<a href=\"../HistoricoTree?Gabeton=" + cb.getLongitude() + "\" target=\"_top\">" + (cb.getLongitude().isEmpty() ? "" : cb.getLongitude()) + "</a>");
//				out.println("<a href=\"../HistoricoTree?Gabeton=" + hb.getIdGabinete() + "&Aplicacion=" + hb.getIdGaveta() + "&numcliente=" + hb.getNumCliente() + "&nomcliente=" + hb.getNomCliente() + "\" target=\"_top\">" + hb.getNomCliente() + "</a>");
				out.println("</td>");
				out.println("<td align=\"left\" width=\"5%\">");
				out.println("<a href=\"../HistoricoTree?Gabeton=" + cb.getType_service() + "\" target=\"_top\">" + (cb.getType_service().isEmpty() ? "" : cb.getType_service()) + "</a>");
//				out.println("<a href=\"../HistoricoTree?Gabeton=" + hb.getIdGabinete() + "&Aplicacion=" + hb.getIdGaveta() + "&numcliente=" + hb.getNumCliente() + "&nomcliente=" + hb.getNomCliente() + "\" target=\"_top\">" + hb.getNomCliente() + "</a>");
				out.println("</td>");
				out.println("<td align=\"left\" width=\"5%\">");
				out.println("<a href=\"../HistoricoTree?Gabeton=" + cb.getMac_device() + "\" target=\"_top\">" + (cb.getMac_device().isEmpty() ? "" : cb.getMac_device()) + "</a>");
//				out.println("<a href=\"../HistoricoTree?Gabeton=" + hb.getIdGabinete() + "&Aplicacion=" + hb.getIdGaveta() + "&numcliente=" + hb.getNumCliente() + "&nomcliente=" + hb.getNomCliente() + "\" target=\"_top\">" + hb.getNomCliente() + "</a>");
				out.println("</td>");
				out.println("<td align=\"left\" width=\"5%\">");
				out.println("<a href=\"../HistoricoTree?Gabeton=" + cb.getEquipment() + "\" target=\"_top\">" + (cb.getEquipment().isEmpty() ? "" : cb.getEquipment()) + "</a>");
//				out.println("<a href=\"../HistoricoTree?Gabeton=" + hb.getIdGabinete() + "&Aplicacion=" + hb.getIdGaveta() + "&numcliente=" + hb.getNumCliente() + "&nomcliente=" + hb.getNomCliente() + "\" target=\"_top\">" + hb.getNomCliente() + "</a>");
				out.println("</td>");
				out.println("<td align=\"left\" width=\"5%\">");
				out.println("<a href=\"../HistoricoTree?Gabeton=" + cb.getPack() + "\" target=\"_top\">" + (cb.getPack().isEmpty() ? "" : cb.getPack()) + "</a>");
//				out.println("<a href=\"../HistoricoTree?Gabeton=" + hb.getIdGabinete() + "&Aplicacion=" + hb.getIdGaveta() + "&numcliente=" + hb.getNumCliente() + "&nomcliente=" + hb.getNomCliente() + "\" target=\"_top\">" + hb.getNomCliente() + "</a>");
				out.println("</td>");
				out.println("</tr>");
			}
			
			out.println("</tbody>");
			out.println("</table>");

		} finally {
			if (out != null)
				out.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletresponseonse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String name = request.getParameter("name");
		response.getWriter().append("Served at: ").append(request.getContextPath())
		.append(" En SearchCustomer")
		.append("name: ").append(name);
	}

}
