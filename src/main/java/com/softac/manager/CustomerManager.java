package com.softac.manager;

import java.util.List;

import com.softac.bean.CustomerBean;
import com.softac.dao.CustomerDao;

public class CustomerManager {

	public void addCustomer(String name, String direction, String location, String phone, String reference, String altitude, 
			String longitude, String type_service, String mac_device, String equipement, String pack) {
		CustomerDao cdo = new CustomerDao();
		cdo.addCustomer(name, direction, location, phone, reference, altitude, longitude, type_service, mac_device, equipement, pack);
	}
	
	public List<CustomerBean> findCustomer(String name) {
		CustomerDao cdo = new CustomerDao();
		List<CustomerBean> listaHistoricos = cdo.findCustomer(name);
		return listaHistoricos;
	}
}
