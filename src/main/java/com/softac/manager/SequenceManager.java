package com.softac.manager;

import static java.lang.String.format;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.softac.db.DataSourceManager;
import com.softac.utils.Sequence;

public class SequenceManager extends DataSourceManager{

	
	private static SequenceManager seqMangr = null;


	public static SequenceManager getInstance() {
		if (seqMangr == null) {
			seqMangr = new SequenceManager();
		}

		return seqMangr;
	}
	

	public static int delete(Connection conn, String name) throws SQLException {

		int retVal = 0;
		PreparedStatement psDelete = null;

		try {
			psDelete = conn.prepareStatement("DELETE FROM sequence WHERE seq_name = ?");

			psDelete.setString(1, name);

			retVal = psDelete.executeUpdate();
		} finally {
			try {
				if (psDelete != null)
					psDelete.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return retVal;
	}

	public static Sequence insert(Connection conn, String name, int value) throws SQLException {

		Sequence retVal = null;
		PreparedStatement psInsert = null;

		try {
			psInsert = conn.prepareStatement("INSERT INTO sequence (seq_name, seq_value) VALUES (?, ?)");

			psInsert.setString(1, name);
			psInsert.setInt(2, value);

			if (psInsert.executeUpdate() == 1)
				retVal = new Sequence(name, value);
		} finally {
			try {
				if (psInsert != null)
					psInsert.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return retVal;
	}

	public static Sequence update(Connection conn, String name, int value) throws SQLException {

		Sequence retVal = null;
		PreparedStatement psUpdate = null;

		try {
			psUpdate = conn.prepareStatement("UPDATE sequence SET seq_value = ? WHERE seq_name = ?");

			psUpdate.setInt(1, value);
			psUpdate.setString(2, name);

			if (psUpdate.executeUpdate() == 1)
				retVal = new Sequence(name, value);
		} finally {
			try {
				if (psUpdate != null)
					psUpdate.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return retVal;
	}

	public static Sequence select(Connection conn, String name) throws SQLException {

		Sequence retVal = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;

		try {
			psSelect = conn.prepareStatement("SELECT * FROM sequence WHERE seq_name = ?");

			psSelect.setString(1, name);

			rs = psSelect.executeQuery();
			if (rs.next())
				retVal = new Sequence(rs.getString("seq_name"), rs.getInt("seq_value"));
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (psSelect != null)
					psSelect.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			rs = null;
			psSelect = null;
		}

		return retVal;
	}

	public static int maxValue(Connection conn, String tableName, String columnName) throws SQLException {

		int retVal = 1;
		ResultSet rs = null;

		try {
			String query = format("SELECT MAX(%s) FROM %s", columnName, tableName);
			rs = conn.createStatement().executeQuery(query);
			if (rs.next())
				retVal = rs.getInt(1);
		} finally {
			try {
				if (rs != null)
					rs.close();
				
			} catch (Exception e) {
				e.printStackTrace();
			}

			rs = null;
		}

		return retVal;
	}

	public int currentValue(String name) throws SQLException {

		int retVal = 1;
		Connection conn = null;
		PreparedStatement psSelect = null;
		ResultSet rs = null;

		try {
			conn = getConnection();
			synchronized (seqMangr) {
				psSelect = conn.prepareStatement("SELECT seq_value FROM sequence WHERE seq_name = ?");

				psSelect.setString(1, name);

				rs = psSelect.executeQuery();
				if (rs.next())
					retVal = rs.getInt(1);
			}
		} finally {
			
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (psSelect != null)
					psSelect.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			rs = null;
			psSelect = null;
			conn = null;
		}

		return retVal;
	}

	public int nextValue(Connection conn, String name) throws SQLException {

		int retVal = -1;
		PreparedStatement psUpdate = null, psSelect = null;
		ResultSet rs = null;

		try {
			// Bloqueamos el registro incrementando al nuevo valor
			psUpdate = conn.prepareStatement("UPDATE sequence SET seq_value = seq_value + 1 WHERE seq_name = ?");
			psUpdate.setString(1, name);

			psUpdate.executeUpdate();
			// Recuperamos el nuevo valor
			psSelect = conn.prepareStatement("SELECT seq_value FROM sequence WHERE seq_name = ?");
			psSelect.setString(1, name);

			rs = psSelect.executeQuery();
			if (rs.next())
				retVal = rs.getInt("seq_value");

			conn.commit();
		} catch (Exception exc) {
			try {
				conn.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			throw new SQLException(exc);
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (psSelect != null)
					psSelect.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (psUpdate != null)
					psUpdate.close();
			} catch (Exception e) {
				e.printStackTrace();
			}


			rs = null;
			psSelect = null;
			psUpdate = null;
		}

		return retVal;
	}

	public int nextValue(Connection conn, String tableName, String columnName, String seqName) throws SQLException {

		int maxValue = -1;
		
		if(seqName.length() > 20)
			seqName = seqName.substring(0, 20);

		try {
			synchronized (seqMangr) {
				maxValue = nextValue(conn, seqName);
				System.out.println("vengo aqui maxValue " + maxValue);
				// Si no existe la secuencia, se crea
				if (maxValue == -1) {
					// Busca el maximo de tableName en columnName
					maxValue = maxValue(conn, tableName, columnName);
					// Se crea la nueva secuencia
					insert(conn, seqName, maxValue);
					// Se hacen permanentes los cambios
					conn.commit();
					// Se recupera el siguiente valor de la secuencia
					maxValue = nextValue(conn, seqName);
				}
			}
		
		} catch (Exception exc) {
			try {
				conn.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			throw new SQLException(exc);
		
		} 

		return maxValue;
	}
}
